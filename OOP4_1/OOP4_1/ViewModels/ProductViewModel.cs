﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;

namespace OOP4_1.ViewModels
{
    using Resources;
    using Models;
    using System.Windows.Media;

    public class ProductViewModel : Notifier
    {
        private Product _product;

        public string Name
        {
            get { return _product.Title; }
            set
            {
                Name = value;
                NotifyPropertyChanged("Name");
            }
        }
        public ProductType Type { get; private set; }

        public int Price
        {
            get { return _product.Price; }
            set
            {
                Price = value;
                NotifyPropertyChanged("Price");
            }
        }
        public PriceType PriceType
        {
            get
            {
                if (Price > 100)
                    return PriceType.Expensive;
                if (Price > 10)
                    return PriceType.Medium;
                return PriceType.Cheap;
            }
        }

        public ProductViewModel(Product product)
        {
            _product = product;
            if (product is Ring) Type = ProductType.Rings;
            if (product is Bracelet) Type = ProductType.Bracelets;
            if (product is Pendant) Type = ProductType.Pendants;
            if (product is Watches) Type = ProductType.Watches;
        }
    }
}
