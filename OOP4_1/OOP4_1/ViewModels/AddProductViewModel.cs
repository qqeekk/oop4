﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.ViewModels
{
    using OOP4_1.Models;
    using OOP4_1.Resources;
    using System.ComponentModel;

    public class AddProductViewModel : Notifier, IDataErrorInfo
    {
        private ProductsModel _model;
        private Product _product;
        private List<ProductType> _productTypes;
        private ProductType _selectedType;

        private string _title;
        private string _description;
        private string _jInserts;
        private string _pmodel;
        private int? _price;
        private string _brand;
        private double? _weight;

        #region ring
        private string _ringSizes;
        private string _ringMaterial;
        #endregion

        #region bracelet
        private string _braceletSizes;
        private string _braceletMaterial;
        #endregion

        #region pendant
        private string _chainType;
        #endregion

        #region watches
        private string _glassType;
        private string _shellType;
        private string _strapType;
        private string _indexType;
        #endregion

        public string Error { get; }
        public string this[string columnName]
        {
            get
            {
                string mError = string.Empty;
                switch (columnName)
                {
                    case "Price":
                        if (Price == null || Price < 1)
                            mError = "Цена должна быть выражена положительным числом";
                        break;
                    case "Weight":
                        if (Weight == null || Weight < 1)
                            mError = "Вес должен быть выражен положтельны числом";
                        break;
                }
                return mError;
            }
        }

        public AddProductCommand AddProductCommand { get; private set; }
        public Product Product { get; set; }
        public List<ProductType> ProductTypes
        {
            get { return _productTypes; }
            private set
            {
                _productTypes = value;
                SelectedType = _productTypes[0];
                NotifyPropertyChanged("ProductTypes");
            }
        }
        public ProductType SelectedType
        {
            get { return _selectedType; }
            set
            {
                _selectedType = value;
                NotifyPropertyChanged("SelectedType");
            }
        }

        #region product
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyPropertyChanged("Title");
            }
        }
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                NotifyPropertyChanged("Description");
            }
        }
        public string JInserts
        {
            get { return _jInserts; }
            set
            {
                _jInserts = value;
                NotifyPropertyChanged("JInserts");
            }
        }
        public string PModel
        {
            get { return _pmodel; }
            set
            {
                _pmodel = value;
                NotifyPropertyChanged("PModel");
            }
        }
        public int? Price
        {
            get { return _price; }
            set
            {
                _price = value;
                NotifyPropertyChanged("Price");
            }
        }
        public string Brand
        {
            get { return _brand; }
            set
            {
                _brand = value;
                NotifyPropertyChanged("Brand");
            }
        }
        public double? Weight
        {
            get { return _weight; }
            set
            {
                _weight = value;
                NotifyPropertyChanged("Weight");
            }
        }
        #endregion

        #region ring
        public string RingSizes
        {
            get { return _ringSizes; }
            set
            {
                _ringSizes = value;
                NotifyPropertyChanged("RingSizes");
            }
        }
        public string RingMaterial
        {
            get { return _ringMaterial; }
            set
            {
                _ringMaterial = value;
                NotifyPropertyChanged("RingMaterial");
            }
        }
        #endregion

        #region bracelet
        public string BraceletSizes
        {
            get { return _braceletSizes; }
            set
            {
                _braceletSizes = value;
                NotifyPropertyChanged("BraceletSizes");
            }
        }
        public string BraceletMaterial
        {
            get { return _braceletMaterial; }
            set
            {
                _braceletMaterial = value;
                NotifyPropertyChanged("BraceletMaterial");
            }
        }
        #endregion

        #region pendant
        public string ChainType
        {
            get { return _chainType; }
            set
            {
                _chainType = value;
                NotifyPropertyChanged("ChainType");
            }
        }
        #endregion

        #region watches
        public string GlassType
        {
            get { return _glassType; }
            set
            {
                _glassType = value;
                NotifyPropertyChanged("GlassType");
            }
        }
        public string ShellType
        {
            get { return _shellType; }
            set
            {
                _shellType = value;
                NotifyPropertyChanged("ShellType");
            }
        }
        public string StrapType
        {
            get { return _strapType; }
            set
            {
                _strapType = value;
                NotifyPropertyChanged("StrapType");
            }
        }
        public string IndexType
        {
            get { return _indexType; }
            set
            {
                _indexType = value;
                NotifyPropertyChanged("IndexType");
            }
        }
        #endregion

        public void CreateNewProduct()
        {
            if(SelectedType == ProductType.Rings)
            {
                _product = new Ring(Title, Price ?? throw new ArgumentNullException())
                { Sizes = RingSizes, Material = RingMaterial };
            }
            else if(SelectedType == ProductType.Bracelets)
            {
                _product = new Bracelet(Title, Price ?? throw new ArgumentNullException())
                { Sizes = BraceletSizes, Material = BraceletMaterial };
            }
            else if (SelectedType == ProductType.Pendants)
            {
                _product = new Pendant(Title, Price ?? throw new ArgumentNullException())
                { ChainType = ChainType };
            }
            else if (SelectedType == ProductType.Watches)
            {
                _product = new Watches(Title, Price ?? throw new ArgumentNullException())
                { GlassType = GlassType, ShellType = ShellType, StrapType = StrapType, IndexType = IndexType };
            }
            _product.Description = Description;
            _product.JInserts = JInserts;
            _product.Model = PModel;
            _product.Brand = Brand;
            _product.Weight = Weight ?? throw new ArgumentNullException();

            ClearEverything();
            _model.Add(_product);
        }
        public void ClearEverything()
        {
            Title = String.Empty;
            Description = String.Empty;
            JInserts = String.Empty;
            PModel = String.Empty;
            Price = null;
            Brand = String.Empty;
            Weight = null;
            RingSizes = String.Empty;
            RingMaterial = String.Empty;
            BraceletSizes = String.Empty;
            BraceletMaterial = String.Empty;
            ChainType = String.Empty;
            GlassType = String.Empty;
            ShellType = String.Empty;
            StrapType = String.Empty;
            IndexType = String.Empty;
        }

        public AddProductViewModel(ProductsModel model)
        {
            _model = model;

            ProductTypes = new List<ProductType>
            {
                ProductType.Rings, ProductType.Bracelets, ProductType.Pendants, ProductType.Watches
            };
            AddProductCommand = new AddProductCommand(this);
        }

    }
}
