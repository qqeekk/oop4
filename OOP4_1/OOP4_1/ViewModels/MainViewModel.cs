﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.ViewModels
{
    using Resources;
    using Models;
    using System.Windows.Input;

    public class Notifier : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class MainViewModel : Notifier
    {
        private readonly ProductsModel _model;

        private ProductType _selectedType;
        private ObservableCollection<ProductViewModel> _selectedTypeProducts;

        public event EventHandler AddProductViewShowed = delegate { };
        
        public NewProductCommand NewProductCommand { get; private set; }

        public List<ProductType> ProductTypes { get; private set; }
        public ProductType SelectedType
        {
            private get
            {
                return _selectedType;
            }
            set
            {
                if (value != _selectedType)
                {
                    _selectedType = value;
                    _model.Update(_selectedType);
                    NotifyPropertyChanged("SelectedType");
                }
            }
        }
        public ObservableCollection<ProductViewModel> SelectedTypeProducts
        {
            get { return _selectedTypeProducts; }
            private set
            {
                _selectedTypeProducts = value;
                NotifyPropertyChanged("SelectedTypeProducts");
            }
        }

        private void OnProductsUpdated(object sender, EventArgs e )
        {
            var selected = from prod in _model.Products select new ProductViewModel(prod);
            SelectedTypeProducts = new ObservableCollection<ProductViewModel>(selected);
        }

        public MainViewModel(ProductsModel pm)
        {
            _model = pm;
            _model.ProjectUpdated += OnProductsUpdated;
            ProductTypes = new List<ProductType>()
            {
                ProductType.Rings, ProductType.Pendants, ProductType.Bracelets, ProductType.Watches
            };
            NewProductCommand = new NewProductCommand(this);
        }

        public void NewAddWindowExecute()
        {
            AddProductViewShowed(this, EventArgs.Empty);
        }
    }
}
