﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace OOP4_1
{
    using OOP4_1.Views;
    using ViewModels;
    using Models;
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ProductsModel _pm;
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            _pm = new ProductsModel();
            MainViewModel mvm = new MainViewModel(_pm);
            mvm.AddProductViewShowed += mvm_AddProductViewShowed;

            MainWindow view = new MainWindow(mvm);
            view.Show();
        }

        private void mvm_AddProductViewShowed(object sender, EventArgs e)
        {
            AddProductViewModel avm = new AddProductViewModel(_pm);
            AddProductWindow aw = new AddProductWindow(avm);
            aw.ShowDialog();
        }
    }
}
