﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OOP4_1.Resources
{
    using ViewModels;
    public class NewProductCommand : ICommand
    {
        private MainViewModel _vm;

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object obj)
        {
            return true;
        }
        public void Execute(object obj)
        {
            _vm.NewAddWindowExecute();
        }
        public NewProductCommand(MainViewModel vm)
        {
            _vm = vm;
        }
    }
}
