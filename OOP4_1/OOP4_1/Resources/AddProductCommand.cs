﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OOP4_1.Resources
{
    using System.Windows;
    using ViewModels;
    public class AddProductCommand : ICommand
    {
        private AddProductViewModel _vm;

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object obj)
        {
            bool shit = false;
            shit |= string.IsNullOrWhiteSpace(_vm.Title);
            shit |= string.IsNullOrWhiteSpace(_vm.Description);
            shit |= string.IsNullOrWhiteSpace(_vm.JInserts);
            shit |= string.IsNullOrWhiteSpace(_vm.PModel);
            shit |= _vm.Price == null;
            shit |= string.IsNullOrWhiteSpace(_vm.Brand);
            shit |= _vm.Weight == null;

            if (_vm.SelectedType == ProductType.Rings)
            {
                shit |= string.IsNullOrWhiteSpace(_vm.RingSizes);
                shit |= string.IsNullOrWhiteSpace(_vm.RingMaterial);
            }
            if (_vm.SelectedType == ProductType.Bracelets)
            {
                shit |= string.IsNullOrWhiteSpace(_vm.BraceletSizes);
                shit |= string.IsNullOrWhiteSpace(_vm.BraceletMaterial);
            }
            if (_vm.SelectedType == ProductType.Pendants)
            {
                shit |= string.IsNullOrWhiteSpace(_vm.ChainType);
            }
            if (_vm.SelectedType == ProductType.Watches)
            {
                shit |= string.IsNullOrWhiteSpace(_vm.GlassType);
                shit |= string.IsNullOrWhiteSpace(_vm.ShellType);
                shit |= string.IsNullOrWhiteSpace(_vm.StrapType);
                shit |= string.IsNullOrWhiteSpace(_vm.IndexType);
            }

            return !shit;

        }
        public void Execute(object obj)
        {
            _vm.CreateNewProduct();
        }

        private void vm_PropertyChanged(object s, EventArgs e)
        {
            CanExecuteChanged(this, e);
        }

        public AddProductCommand(AddProductViewModel vm)
        {
            _vm = vm;
            _vm.PropertyChanged += vm_PropertyChanged;
        }
    }
}
