﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OOP4_1.Resources
{
    public class ProductTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ProductType? ptVal = value as ProductType?;
            switch (ptVal)
            {
                case ProductType.Rings:
                    return "кольца";
                case ProductType.Watches:
                    return "часы";
                case ProductType.Pendants:
                    return "подвески";
                case ProductType.Bracelets:
                    return "браслеты";
                default:
                    throw new Exception("Unknown ProductType");
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //string stringVal = value as string;
            //switch (stringVal)
            //{
            //    case "часы":
            //        return ProductType.Watches;
            //    case "подвески":
            //        return ProductType.Pendants;
            //    case "браслеты":
            //        return ProductType.Bracelets;
            //    case "кольца":
            //        return ProductType.Rings;
            //    default:
            //        throw new Exception("Unknown ProductType");
            throw new NotImplementedException();
            }
    }
}
