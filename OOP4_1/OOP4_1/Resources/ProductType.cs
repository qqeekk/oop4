﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Resources
{
    public enum ProductType
    {
        Rings = 1,
        Bracelets = 2,
        Pendants = 4,
        Watches = 8
    }
}
