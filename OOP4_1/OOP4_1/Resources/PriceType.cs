﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Resources
{
    public enum PriceType
    {
        Expensive = 0,
        Medium = 1,
        Cheap = 2
    }
}
