﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Models
{
    public abstract class Product
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string JInserts { get; set; }

        public string Model {get; set;}
        public int Price { get; set; }
        public string Brand { get; set; }
        public double Weight { get; set; }

        public Product(string title, int price)
        {
            Title = title;
            Price = price;
        }
    }
}
