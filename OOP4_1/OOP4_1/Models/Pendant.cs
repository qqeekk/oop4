﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Models
{
    public class Pendant : Product
    {
        public string ChainType { get; set; }
        public Pendant(string name, int price)
            : base(name, price) { }
    }
}
