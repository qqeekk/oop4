﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace OOP4_1.Models.IO
{
    public class WatchesReader : ProductsReader
    {
        public WatchesReader(string dataBaseName, string tableName)
            : base(dataBaseName, tableName) { }


        protected override Product GetProduct(NameValueCollection r)
        {
            var watches = new Watches(r["title"], int.Parse(r["price"]));
            watches.Description = r["description"];
            watches.JInserts = r["jInserts"];
            watches.Model = r["model"];
            watches.Brand = r["brand"];
            watches.Weight = double.Parse(r["weight"], CultureInfo.InvariantCulture);

            watches.GlassType = r["glassType"];
            watches.ShellType = r["shellType"];
            watches.StrapType = r["strapType"];
            watches.IndexType = r["indexType"];

            return watches;
        }
    }
}
