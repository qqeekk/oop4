﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace OOP4_1.Models.IO
{
    public class RingsReader : ProductsReader
    {
        public RingsReader(string dataBaseName, string tableName)
            : base(dataBaseName, tableName) { }


        protected override Product GetProduct(NameValueCollection r)
        {
            var ring = new Ring(r["title"].ToString(), int.Parse(r["price"]));
            ring.Description = r["description"].ToString();
            ring.JInserts = r["jInserts"].ToString();
            ring.Model = r["model"].ToString();
            ring.Brand = r["brand"].ToString();
            ring.Weight = double.Parse(r["weight"], CultureInfo.InvariantCulture);

            ring.Sizes = r["sizes"].ToString();
            ring.Material = r["material"].ToString();

            return ring;
        }
    }
}
