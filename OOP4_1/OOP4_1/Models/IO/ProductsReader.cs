﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Collections.Specialized;

namespace OOP4_1.Models.IO
{
    public abstract class ProductsReader
    {
        public string TableName { get; }
        public string DataBaseName { get; }

        public ProductsReader(string dataBaseName, string tableName)
        {
            TableName = tableName;
            DataBaseName = dataBaseName;
        }

        public List<Product> ReadAll()
        {
            List<Product> list = new List<Product>();
            Product product;
            using (var conn = new SQLiteConnection("Data Source=" + DataBaseName + "; Version=3;"))
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                string sql_command = "select * from " + TableName + " order by price desc;";
                var cmd = new SQLiteCommand(sql_command, conn);

                try
                {
                    var r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        try
                        {
                            product = GetProduct(r.GetValues());
                            list.Add(product);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            throw new Exception(string.Empty, ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw new Exception(string.Empty, ex);
                }
            };
            return list;
        }
        protected abstract Product GetProduct(NameValueCollection r_params);
    }
}
