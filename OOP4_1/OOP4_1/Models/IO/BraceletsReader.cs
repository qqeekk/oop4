﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Models.IO
{
    public class BraceletsReader : ProductsReader
    {
        public BraceletsReader(string dataBaseName, string tableName)
            : base(dataBaseName, tableName) { }


        protected override Product GetProduct(NameValueCollection r)
        {
            var bracelet = new Bracelet(r["title"].ToString(), int.Parse(r["price"]));
            bracelet.Description = r["description"].ToString();
            bracelet.JInserts = r["jInserts"].ToString();
            bracelet.Model = r["model"].ToString();
            bracelet.Brand = r["brand"].ToString();
            bracelet.Weight = double.Parse(r["weight"], CultureInfo.InvariantCulture);

            bracelet.Sizes = r["sizes"].ToString();
            bracelet.Material = r["material"].ToString();

            return bracelet;
        }
    }
}
