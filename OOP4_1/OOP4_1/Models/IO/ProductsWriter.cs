﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace OOP4_1.Models.IO
{
    public abstract class ProductsWriter
    {
        public string TableName { get; }
        public string DataBaseName { get; }

        public ProductsWriter(string dataBaseName, string tableName)
        {
            TableName = tableName;
            DataBaseName = dataBaseName;
        }

        public void Write(Product product)
        {
            using (var conn = new SQLiteConnection("Data Source=" + DataBaseName + "; Version=3;"))
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                string sql_command = GetCommand(product);
                var cmd = new SQLiteCommand(sql_command, conn);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw new Exception(string.Empty, ex);
                }
            };
        }
        protected abstract string GetCommand(Product product);
    }
}
