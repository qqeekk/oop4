﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Models.IO
{
    public class PendantsReader : ProductsReader
    {
        public PendantsReader(string dataBaseName, string tableName)
            : base(dataBaseName, tableName) { }


        protected override Product GetProduct(NameValueCollection r)
        {
            var pendant = new Pendant(r["title"], int.Parse(r["price"]));
            pendant.Description = r["description"];
            pendant.JInserts = r["jInserts"];
            pendant.Model = r["model"];
            pendant.Brand = r["brand"];
            pendant.Weight = double.Parse(r["weight"], CultureInfo.InvariantCulture);

            pendant.ChainType = r["chainType"];

            return pendant;
        }
    }
}
