﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Models.IO
{
    public class PendantsWriter : ProductsWriter
    {
        public PendantsWriter(string dataBaseName, string tableName)
            : base(dataBaseName, tableName) { }

        protected override string GetCommand(Product product)
        {
            Pendant p = product as Pendant;
            string format
                = "insert into "
                + TableName
                + "(title, description, jInserts, model, price, "
                + "brand, weight, chainType) values "
                + "('{0}', '{1}', '{2}', '{3}', {4}, "
                + "'{5}', {6}, '{7}');";
            return string.Format(format, p.Title, p.Description, p.JInserts, p.Model, p.Price, p.Brand, p.Weight,
                p.ChainType);
        }
    }
}
