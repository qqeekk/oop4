﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Models
{
    public class Watches : Product
    {
        public string GlassType { get; set; }
        public string ShellType { get; set; }
        public string StrapType { get; set; }
        public string IndexType { get; set; }

        public Watches(string name, int price)
            : base(name, price) { }
    }
}
