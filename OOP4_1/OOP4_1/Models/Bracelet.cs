﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_1.Models
{
    public class Bracelet : Product
    {
        public string Sizes { get; set; }
        public string Material {get; set;}

        public Bracelet(string name, int price)
            : base(name, price) { }
    }
}
