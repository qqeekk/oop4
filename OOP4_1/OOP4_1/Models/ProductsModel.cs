﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace OOP4_1.Models
{
    using Resources;
    using IO;
    public class ProductsModel
    {
        private string _sqlProd_CreateParam 
            = "id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "title TEXT, "
            + "description TEXT, "
            + "jInserts TEXT, "
            + "model TEXT, "
            + "price INTEGER, "
            + "brand TEXT, "
            + "weight DOUBLE";

        private void _initializeProductTable(string dataBase, string tableName, string appendix)
        {
            using (var conn = new SQLiteConnection("Data Source=" + dataBase + "; Version=3;"))
            {
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                string sql_command = "CREATE TABLE IF NOT EXISTS " 
                    + tableName
                    + "("
                    + _sqlProd_CreateParam 
                    + ", "
                    + appendix
                    + ");";
                var cmd = new SQLiteCommand(sql_command, conn);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private ProductType _selectedType;

        private ObservableCollection<Product> _products { get; set; }
        public ReadOnlyObservableCollection<Product> Products { get; private set; }

        public void Update(ProductType tp)
        {
            if (tp == _selectedType)
                return;
            _selectedType = tp;

            ProductsReader r;

            switch(tp)
            {
                case ProductType.Rings:
                    r = new RingsReader("../../Files/database.db", "rings");
                    break;
                case ProductType.Bracelets:
                    r = new BraceletsReader("../../Files/database.db", "bracelets");
                    break;
                case ProductType.Pendants:
                    r = new PendantsReader("../../Files/database.db", "pendants");
                    break;
                case ProductType.Watches:
                    r = new WatchesReader("../../Files/database.db", "watches");
                    break;
                default:
                    throw new Exception("Unknown Type");
            }
            _products = new ObservableCollection<Product>(r.ReadAll());
            Products = new ReadOnlyObservableCollection<Product>(_products);
            ProjectUpdated(this, EventArgs.Empty);
        }

        public event EventHandler ProjectUpdated = delegate { };

        public ProductsModel()
        {
            _initializeProductTable("../../Files/database.db", "rings", "sizes TEXT, material TEXT");
            _initializeProductTable("../../Files/database.db", "bracelets", "sizes TEXT, material TEXT");
            _initializeProductTable("../../Files/database.db", "pendants", "chainType TEXT");
            _initializeProductTable("../../Files/database.db", "watches", "glassType TEXT, shellType TEXT, strapType TEXT, indexType TEXT");
        }
        public void Add(Product product)
        {
            //_products.Add(product);

            ProductsWriter pw;

            if (product is Ring)
            {
                pw = new RingsWriter("../../Files/database.db", "rings");
                if (_selectedType == ProductType.Rings) _products.Add(product);
            }
                
            else if (product is Pendant)
            {
                pw = new PendantsWriter("../../Files/database.db", "pendants");
                if (_selectedType == ProductType.Pendants) _products.Add(product);
            }
            else if (product is Bracelet)
            {
                pw = new BraceletsWriter("../../Files/database.db", "bracelets");
                if (_selectedType == ProductType.Bracelets) _products.Add(product);
            }
                
            else if (product is Watches)
            {
                pw = new WatchesWriter("../../Files/database.db", "watches");
                if (_selectedType == ProductType.Watches) _products.Add(product);
            }
                
            else
                throw new Exception("Unknown Type");

            pw.Write(product);

            ProjectUpdated(this, EventArgs.Empty);
        }
    }
}
