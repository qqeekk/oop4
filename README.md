# lab 4 OOP
### About
A small single-page WPF application. Adding entities of different types in a database and watching them added:)

Application is done with the use of MVVM pattern via WPF + XAML + C#.

### Here's a GUI
![Rings](https://pp.userapi.com/c830609/v830609594/d5fba/eTKFveA87WY.jpg)
![Adding entity](https://pp.userapi.com/c830609/v830609594/d5fcb/Cibo19jFEls.jpg)